
#define SensorPin 0

bool sendValue = false;
int sleep = 1;
unsigned long start_time;

void setup() {
  pinMode(13, OUTPUT);
  Serial.begin(9600);
  start_time = millis();
}

void loop() {
  if (Serial.available() > 0) {
    unsigned char cmd = Serial.read();
    if (cmd == 104) {
      sendValue = false;
    } else if (cmd == 115) {
      sendValue = true;
    } else if (cmd == 0) {
      sleep = 1;      
    } else if (cmd == 1) {
      sleep = 200;
    } else if (cmd == 2) {
      sleep = 1000;
    } else if (cmd == 3) {
      sleep = 5000;
    } else if (cmd == 4) {
      sleep = 1000;
    }
  }

  unsigned long now_time = millis();
  if (now_time - start_time >= sleep) {
    int value = analogRead(SensorPin);
    float data = 0.01368524 * value;
    byte *s_data = (byte *) &data;
    if (sendValue) {
      Serial.write(s_data, 4);
      digitalWrite(13, HIGH);
      delay(100);
      digitalWrite(13, LOW);   
    } 
    start_time = now_time;
  }
}
