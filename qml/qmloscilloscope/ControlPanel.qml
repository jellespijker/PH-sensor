/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the Qt Charts module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 2.7
import QtQuick.Layouts 1.0
import QtQuick.Controls 2.2
import QtQuick.Extras 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.2

ColumnLayout {
    spacing: 8
    Layout.fillHeight: true
    width: 150
    property alias circularGaugeValue: circularGauge.value

    FileDialog {
        id: fileDialog
        title: "Please choose a file"
        folder: shortcuts.home
        selectExisting: false
        nameFilters: ["Comma Sepperated Values (*.csv)", "All files (*)" ]
        onAccepted: {
            dataSource.exportToCsv(fileDialog.fileUrl)
        }
        Component.onCompleted: visible = false
    }

    CircularGauge {
        id: circularGauge
        width: parent.width
        height: parent.width
        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
        antialiasing: true
        Layout.fillHeight: false
        Layout.fillWidth: true
        stepSize: 0
        maximumValue: 14
        value: dataSource.ActualValue
        tickmarksVisible: true
        style: CircularGaugeStyle {
            labelStepSize: 1
        }
    }

    ComboBox {
        id: portsList
        objectName: "portsList"
        currentIndex: 0
        model: myModel
        Layout.fillWidth: true
        antialiasing: true
        onActivated: {
            dataSource.on_portChanged(currentText)
            connectBtn.checked = false
            connectBtn.text = "Connect"
        }
    }

    Button {
        id: connectBtn
        width: 200
        text: qsTr("Connect")
        checkable: true
        Layout.fillWidth: true
        antialiasing: true
        onClicked: {
            if (connectBtn.checked) {
                connectBtn.text = "Disconnect"
                dataSource.connectSerial()
            } else {
                connectBtn.text = "Connect"
                dataSource.disconnectSerial()
            }
        }
    }


    Button {
        id: logBtn
        text: qsTr("Start logging")
        enabled: dataSource.Connected
        checkable: true
        Layout.fillWidth: true
        onClicked: {
            if (logBtn.checked) {
                logBtn.text = "Stop logging"
                dataSource.log()
            } else {
                logBtn.text = "Start logging"
                dataSource.stopLog()
            }
        }
    }

    Button {
        id: exportBtn
        text: qsTr("Export")
        enabled: dataSource.Exportable
        Layout.fillWidth: true
        onClicked: {
            fileDialog.open()
        }
    }

    ComboBox {
        id: refreshList
        enabled: dataSource.Connected
        objectName: "refreshList"
        currentIndex: 0
        model: ["Real-time", "5 Hz", "1 Hz", "1/5 Hz", "1/10 Hz"]
        Layout.fillWidth: true
        antialiasing: true
        onActivated: {
            dataSource.on_refreshChanged(refreshList.currentIndex)
        }
    }

    Rectangle {
        id: rectangle
        width: 200
        height: 200
        color: "#ffffff"
        opacity: 0
        visible: true
        Layout.fillHeight: true
        Layout.fillWidth: true
        Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom
    }

}
